(function () {
    'use strict';

    angular
        .module('todoApp')
        .controller('todoController', controller);

    function controller(todoService, $rootScope) {
        /* jshint validthis:true */
        var vm = this;
		
        vm.todos = {};
        
        vm.refreshTodoItems = refreshTodoItems;
        vm.createItem = createItem;
        vm.isCompletedChanged = isCompletedChanged;

        activate();

        function activate() {
			refreshTodoItems();
		}
        
        function refreshTodoItems() {
            return todoService.getAll()
                .then(function(todoItems) {
                    vm.todos = todoItems;
                })
                .finally(function() {
                    // Stop the ion-refresher from spinning
                    $rootScope.$broadcast('scroll.refreshComplete');
                });
        }
        
        function createItem() {
            console.log("a");
            return todoService.create({ text: vm.newItemText })
                .then(function(createdItem) {
                    vm.todos.push(createdItem);
                    vm.newItemText = '';
                });
        }
        
        function isCompletedChanged(todo) {
            return todoService.update(todo);
        }
    }
})();
