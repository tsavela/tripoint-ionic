(function () {
    'use strict';

    angular
        .module('todoApp')
        .factory('todoService', todoService);

    function todoService($http) {
        var service = {
            getAll: getAll,
            update: update,
            create: create
        };
        
        var baseUrl = 'http://localhost:3000';

        return service;

        function getAll() {
            return $http.get(baseUrl + '/api/todo')
                .then(function(response) {
                    return response.data;
                });
        }
        
        function update(todoItem) {
            return $http.put(baseUrl + '/api/todo/' + todoItem._id, todoItem)
                .then(function(response) {
                    return response.data;
                });
        }
        
        function create(todoItem) {
            return $http.post(baseUrl + '/api/todo', todoItem)
                .then(function(response) {
                    return response.data;
                });
        }
    }
})();